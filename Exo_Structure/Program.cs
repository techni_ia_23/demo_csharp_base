﻿#region Énoncé
// Écrire une structure pour définir un personnage : il possède un charactère permettant de le représenter (N si nain, H si humain, E si elfe, L si loup, O si orque, D si dragonnet) et deux entiers X et Y représentant ses coordonnées.

// Créer une collection/tableau de personnages (7 perso)
// Pour remplir le tableau de personnages, demandez à l'utiliser quel personnage il souhaite jouer (N H E) (Ses coordonnées seront mises en haut à gauche du plateau)
// Faites ensuite 2 loups, 2 orques, 2 dragonnets et générez leur position aléatoirement (X, Y) (Attention, si la place est déjà prise, on recommence) (Le plateau fera 5 sur 5)

// Affichez un plateau de 5 sur 5.
// Placez vos personnages dans le plateau. (Utilisez Console.SetCursorPosition(x, y))
#endregion

#region 1) Création Struc Personnage [v]
using Exo_Structure;
#endregion

#region 2) Création collection/tableau de personnages [v]
List<Personnage> personnages = new List<Personnage>();

// Ou 
//Personnage[] personnages = new Personnage[7];
#endregion

#region 3) Création/Ajout personnage joueur [v]
string choixUser;
do
{
    Console.Clear();
    Console.WriteLine("Quelle race voulez-vous jouer ?");
    Console.WriteLine("1) Nain");
    Console.WriteLine("2) Humain");
    Console.WriteLine("3) Elfe");
    choixUser = Console.ReadLine() ?? "";

} while (!(choixUser == "1" || choixUser == "2" || choixUser == "3"));
Personnage hero;
switch(choixUser)
{
    case "1":
        hero.Race = 'N';
        Console.WriteLine("Vous avez choisi Nain !");

        break;
    case "2":
        hero.Race = 'H';
        Console.WriteLine("Vous avez choisi Humain !");
        break;
    case "3":
        hero.Race = 'E';
        Console.WriteLine("Vous avez choisi Elfe !");
        break;
    default:
        hero.Race = '?';
        Console.WriteLine("Unexpected error !");
        break;
}
hero.X = 0;
hero.Y = 0;
personnages.Add(hero);
#endregion

#region 4) Création/Ajout monstres [v]
Random rand = new Random();
for (int i = 0; i < 6 ; i++)
{
    Personnage monstreAAjouter;
    if(i < 2)
    {
        monstreAAjouter.Race = 'L';
    }
    else if( i < 4)
    {
        monstreAAjouter.Race = 'O';
    }
    else
    {
        monstreAAjouter.Race = 'D';
    }

    bool positionOk;
    int testX;
    int testY;
    do
    {
        positionOk = true;
        testX = rand.Next(5);
        testY = rand.Next(5);
        foreach (Personnage p in personnages)
        {
            if (p.X == testX && p.Y == testY)
            {
                positionOk = false;
            }
        }

    } while (!positionOk); //Tant que positionOk est faux, on génère des positions
    monstreAAjouter.X = testX;
    monstreAAjouter.Y = testY;
    Console.WriteLine($"Ajout du monstre N°{i+1}- {monstreAAjouter.Race} - {monstreAAjouter.X} - {monstreAAjouter.Y}");
    personnages.Add(monstreAAjouter);

}
#endregion

#region 5) Affichage plateau [v]
Console.Clear();
for(int i = 0; i < 5 ; i++)
{
    for(int j = 0;  j < 5 ; j++)
    {
        Console.Write("_"); 
    }
    Console.WriteLine();
}
#endregion

#region 6) Affichage personnages [v]
foreach(Personnage p in personnages)
{
    Console.SetCursorPosition(p.X, p.Y);
    Console.Write(p.Race);
}
#endregion
Console.SetCursorPosition(0, 8);
Console.WriteLine("Appuyez sur n'importe quelle touche pour sortir");
Console.ReadLine();

