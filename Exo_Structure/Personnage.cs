﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo_Structure
{
    public struct Personnage
    {
        public char Race;
        public int X;
        public int Y;

        //public Coordonnees Coordonnees;
    }

    //Autre option :
    //public struct Coordonnees
    //{
    //    public int X;
    //    public int Y;
    //}
}
