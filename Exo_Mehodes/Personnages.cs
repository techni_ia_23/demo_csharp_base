﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo_Mehodes
{
    public struct Personnage
    {
        public char Race;
        public int X;
        public int Y;

        public void Afficher()
        {
            Console.SetCursorPosition(X, Y);
            Console.Write(Race);
        }
        public void Effacer()
        {
            Console.SetCursorPosition(X, Y);
            Console.Write("_");
        }
    }
}
