﻿using Exo_Mehodes;

List<Personnage> personnages = new List<Personnage>();

string choix = SetUpChoixUser();
Personnage hero = SetUpHero(choix);
personnages.Add(hero);

SetUpListMonstres(personnages);

AfficherPlateau();

foreach (Personnage p in personnages)
{
    p.Afficher();
}

Console.SetCursorPosition(0, 8);
Console.WriteLine("Appuyez sur n'importe quelle touche pour sortir");
Console.ReadLine();
// -----------------  NOS METHODES -----------------
string SetUpChoixUser()
{
    string choixUser;
    do
    {
        Console.Clear();
        Console.WriteLine("Quelle race voulez-vous jouer ?");
        Console.WriteLine("1) Nain");
        Console.WriteLine("2) Humain");
        Console.WriteLine("3) Elfe");
        choixUser = Console.ReadLine() ?? "";

    } while (!(choixUser == "1" || choixUser == "2" || choixUser == "3"));
    return choixUser;
}

Personnage SetUpHero(string choix)
{
    Personnage hero;
    switch (choix)
    {
        case "1":
            hero.Race = 'N';
            Console.WriteLine("Vous avez choisi Nain !");

            break;
        case "2":
            hero.Race = 'H';
            Console.WriteLine("Vous avez choisi Humain !");
            break;
        case "3":
            hero.Race = 'E';
            Console.WriteLine("Vous avez choisi Elfe !");
            break;
        default:
            hero.Race = '?';
            Console.WriteLine("Unexpected error !");
            break;
    }
    hero.X = 0;
    hero.Y = 0;
    return hero;
}

bool CheckPosition(int X, int Y, List<Personnage> personnages)
{
    foreach (Personnage p in personnages)
    {
        if (p.X == X && p.Y == Y)
        {
            return false;
        }
    }
    return true;
}
void SetUpListMonstres(List<Personnage> personnages)
{
    Random rand = new Random();
    for (int i = 0; i < 6; i++)
    {
        Personnage monstreAAjouter;
        if (i < 2)
        {
            monstreAAjouter.Race = 'L';
        }
        else if (i < 4)
        {
            monstreAAjouter.Race = 'O';
        }
        else
        {
            monstreAAjouter.Race = 'D';
        }

        bool positionOk;
        int testX;
        int testY;
        do
        {
            testX = rand.Next(5);
            testY = rand.Next(5);
            positionOk = CheckPosition(testX, testY, personnages);

        } while (!positionOk); //Tant que positionOk est faux, on génère des positions
        monstreAAjouter.X = testX;
        monstreAAjouter.Y = testY;
        Console.WriteLine($"Ajout du monstre N°{i + 1}- {monstreAAjouter.Race} - {monstreAAjouter.X} - {monstreAAjouter.Y}");
        personnages.Add(monstreAAjouter);

    }
}

void AfficherPlateau()
{
    Console.Clear();
    for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j < 5; j++)
        {
            Console.Write("_");
        }
        Console.WriteLine();
    }
}

