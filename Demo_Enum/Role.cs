﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_Enum
{
    [Flags]
    public enum Role
    {
        Visitor = 1,
        User = 2,
        Moderator = 4,
        Administrator = 8
    }
}
