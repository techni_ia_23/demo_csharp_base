﻿using Demo_Enum;

Console.WriteLine("Demo Enum");
Role monRole = Role.Visitor | Role.User | Role.Moderator;

Console.WriteLine("Mon role est :");
Console.WriteLine(monRole.ToString());
Console.WriteLine((int)monRole);

if (monRole.HasFlag(Role.Visitor))
{
    Console.WriteLine("J'ai seulement le droit de parcourir le site");
}
if (monRole.HasFlag(Role.User))
{
    Console.WriteLine("Je suis connecté, je peux utiliser les fonctionnalités du site");
}
if (monRole.HasFlag(Role.Moderator))
{
    Console.WriteLine("Je suis connecté, je peux supprimer des messages et modérer des sujets");
}
if (monRole.HasFlag(Role.Administrator))
{
    Console.WriteLine("Je suis connecté, je peux tout faire, je suis le king ");
}

// Je veux voir toutes les valeurs possibles de mon Enum
string[] tableauDesNoms = Enum.GetNames(typeof(Role));
foreach(string name in tableauDesNoms)
{
    Console.WriteLine(name);
}

string roleUser = "Visitor";
Role realRoleUser;
Enum.TryParse<Role>(roleUser, out realRoleUser);