﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_Methodes
{
    public struct Personne
    {
        public string Nom;
        public string Prenom;
        public int AnneeNaissance;
        public void DireBonjour()
        {

            Console.WriteLine($"Bonjour je m'appelle {Prenom} {Nom}");
        }
        public void DireBonjour(string langue)
        {
            if(langue == "fr")
            {
                Console.WriteLine("Bonjour");
            }
            else
            {
                Console.WriteLine("Compren po");
            }
        }
        public void DireBonjour(string langue, string pays)
        {

        }

        public string Majeur()
        {
            if(2023 - AnneeNaissance >= 18)
            {
                return "Majeur";
            }
            return "Mineur";
        }

        public string DireBonjourLanguePays(string langue = "fr", string pays = "Belgique")
        {
            switch(langue)
            {
                case "fr":
                    return "Bonjour de " + pays;
                case "en":
                    return "Hello from " + pays;
                case "nl":
                    return "Hallo van "+ pays;
                default:
                    return "Je ne parle pas votre langue";
            }
        }

        public double CalculMoyenne(params int[] notes)
        {
            double total = 0;
            foreach(int note in notes)
            {
                total += note;
            }
            return total/ notes.Length;
        }

        public void AfficherAnimaux(bool aAnimal, params string[] noms)
        {
            if(!aAnimal)
            {
                Console.WriteLine("N'a pas d'animal");
            }
            else
            {
                Console.WriteLine("Ses animaux sont :");
                foreach (string nom in noms)
                {
                    Console.WriteLine("- " + nom);
                }
            }
        }

        public void modifNombre(ref int x)
        {
            x++;
            Console.WriteLine("DANS LA METHODE : " + x);
        }
    }
}
