﻿using Demo_Methodes;

Console.WriteLine("Demo Méthodes");

Personne p;
p.Nom = "Strimelle";
p.Prenom = "Aurélien";
p.AnneeNaissance = 1989;
p.DireBonjour();
Console.WriteLine($"Je suis {p.Majeur()} !");
Console.WriteLine($"La moyenne de Aurélien est de : {p.CalculMoyenne(18, 17, 15, 16, 13, 14)}");
p.AfficherAnimaux(true, "Riri", "Fifi", "Loulou");
int x = 6;
p.modifNombre(ref x);
Console.WriteLine("DANS LE PROGRAMME : " + x);

Personne p2;
p2.Nom = "Beurive";
p2.Prenom = "Aude";
p2.AnneeNaissance = 1989;
p2.DireBonjour();
p2.DireBonjour("be");
p2.AfficherAnimaux(false);


string? jour = "Jeudi";
Console.WriteLine(jour?.Length); //Opérateur null conditionnel : N'applique la méthode/va cercher la valeur de la proriété uniquement sur l'objet n'est pas null (ne fera rien du tout si null)
Console.WriteLine((jour is not null) ? jour.Length : "Pas de jour entré"); //Ternaire : On vérifie à la main si l'objet n'est pas null, sinon on fait autre chose
Console.WriteLine(jour ?? "Pas de jour entré" ); //Coalesce : Va chercher la valeur de l'objet sinon fait autre chose
string langue = "fr";
Console.WriteLine(p.DireBonjourLanguePays(langue, "Allemagne"));
Console.WriteLine(p.DireBonjourLanguePays(pays:"Allemagne", langue:"fr"));
Console.WriteLine(p.DireBonjourLanguePays("nawak"));
Console.WriteLine(p.DireBonjourLanguePays()); //Prendra la valeur par défaut fr

int nombre;
string entreeUser;
do
{
    Console.WriteLine("Rentre un nombre");
    entreeUser = Console.ReadLine();

} while (!int.TryParse(entreeUser, out nombre));

Console.ReadLine();
